var navbarDropdown = UIkit.drop(".uk-navbar-dropdown-nav", {
    mode: "click"
});

var isMenuOpen = false;

function openFirstMenuItem() {
    $(".uk-open + .uk-navbar-dropdown .uk-navbar-dropdown__content > div:first > button").trigger("click");
    isMenuOpen = true;
}

$(".uk-navbar-nav > li > a").on("mouseenter", function() {
    if ($(".uk-navbar-dropdown .uk-open").length === 0 && !isMenuOpen) {
        openFirstMenuItem();
    }
    isMenuOpen = false;
})

$("#mobile-menu .uk-navbar-nav > li > a").on("click", function(event){
    event.preventDefault();
})
